// Do not insist on coverage of test code
// istanbul ignore file

import 'jasmine';
import * as Joi from 'joi';

type SchemaWithPrivates = Joi.Schema & { $_terms: { examples: Array<string> } };

/**
 * Creates tests that make sure `schema` is a serious schema.
 *
 * Use inside a `describe`.
 */
export const expectSeriousSchema = (
  schema: Joi.Schema,
  failures: Array<any>,
  unknownNotAllowed = false,
  context: {} | undefined = undefined
): void => {
  const schemaWithPrivates: SchemaWithPrivates = schema as SchemaWithPrivates;
  it('is a Joi schema', () => {
    expect(schema).toBeDefined();
    expect(Joi.isSchema(schema)).toBe(true);
  });
  it('has a description', () => {
    expect(typeof schema._flags).toBe('object');
    expect(typeof schema._flags.description).toBe('string');
    expect(schema._flags.description).not.toBe('');
    expect(schema._flags.description).toMatch(/\w+/);
  });
  it('has at least one example', () => {
    expect(schemaWithPrivates.$_terms).toBeDefined();
    expect(schemaWithPrivates.$_terms.examples).toBeDefined();
    expect(Array.isArray(schemaWithPrivates.$_terms.examples)).toBe(true);
    expect(schemaWithPrivates.$_terms.examples.length).toBeGreaterThan(0);
  });
  if (schema.type === 'object') {
    if (!unknownNotAllowed) {
      it('allows unknown keys (allow for server evolution)', () => {
        expect(typeof schema._flags).toBe('object');
        expect(schema._flags.unknown).toBe(true);
      });
    } else {
      it('does not allow unknown keys', () => {
        expect(typeof schema._flags).toBe('object');
        expect(schema._flags.unknown).toBeFalsy();
      });
    }
  }
  describe('examples', () => {
    if (
      schemaWithPrivates.$_terms !== undefined &&
      schemaWithPrivates.$_terms.examples !== undefined &&
      schemaWithPrivates.$_terms.examples.forEach !== undefined
    ) {
      schemaWithPrivates.$_terms.examples.forEach((ex, index) => {
        it(`${index}: example ${JSON.stringify(ex)} passes the schema`, () => {
          expect(schema.validate(ex, { context }).error).toBeUndefined();
        });
      });
    }
  });
  describe('failures', () => {
    failures.forEach((f, i) => {
      it(`${i}: fails for ${JSON.stringify(f)}`, () => {
        expect(schema.validate(f, { context }).error).toBeDefined();
      });
    });
  });
};
